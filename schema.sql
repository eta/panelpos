PRAGMA foreign_keys = ON;

CREATE TABLE stock (
    code TEXT PRIMARY KEY,
    name TEXT NOT NULL,
    price_pence INT NOT NULL,
    qty INT NOT NULL
);

CREATE TABLE orders (
    id TEXT PRIMARY KEY,
    order_ts TEXT NOT NULL,
    email TEXT NOT NULL,
    phone_number TEXT NOT NULL,
    address TEXT NOT NULL,
    payment_intent_id TEXT NOT NULL,
    confirmed BOOL NOT NULL DEFAULT FALSE
);

CREATE TABLE order_stock (
    order_id TEXT NOT NULL REFERENCES orders,
    stock_id TEXT NOT NULL REFERENCES stock,
    qty INT NOT NULL,
    UNIQUE(order_id, stock_id)
);