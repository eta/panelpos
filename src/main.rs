use crate::cfg::Config;
use axum::extract::State;
use axum::routing::get;
use axum::Router;
use std::sync::Arc;
use std::{env, fs};

mod cfg;
mod db;

struct AppState {
    stripe: stripe::Client,
}

async fn root(State(app): State<Arc<AppState>>) -> &'static str {
    "hello from the panel cash obtaining zone"
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let config_path = env::args().nth(1).unwrap_or_else(|| "./config.toml".into());
    eprintln!("[+] reading config at {config_path}...");
    let config_toml = fs::read_to_string(&config_path)?;
    let config: Config = toml::from_str(&config_toml)?;

    let app = Arc::new(AppState {
        stripe: stripe::Client::new(config.stripe_api_key),
    });

    let web_zone = Router::new().route("/", get(root)).with_state(app);

    let listener = tokio::net::TcpListener::bind("[::]:4000").await?;
    axum::serve(listener, web_zone).await?;
    Ok(())
}
