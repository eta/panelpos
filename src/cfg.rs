//! Application configuration.

use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    pub stripe_api_key: String,
    pub listen: String,
}
