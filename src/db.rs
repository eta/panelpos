//! Database management.

use anyhow::anyhow;
use rusqlite::{params, Connection, Transaction, TransactionBehavior};
use time::OffsetDateTime;
use uuid::Uuid;

#[derive(Clone, Debug, PartialEq)]
pub struct Stock {
    pub code: String,
    pub name: String,
    pub price_pence: u32,
    pub qty: u32,
}

impl Stock {
    pub fn get(code: &str, conn: &Connection) -> anyhow::Result<Stock> {
        let mut get_stock = conn.prepare("SELECT * FROM stock WHERE code = ?")?;
        let ret = get_stock
            .query_map([code], |r| {
                Ok(Self {
                    code: r.get(0)?,
                    name: r.get(1)?,
                    price_pence: r.get(2)?,
                    qty: r.get(3)?,
                })
            })?
            .next()
            .ok_or_else(|| anyhow!("No stock with code {code}"))??;

        Ok(ret)
    }

    pub fn remaining(&self, conn: &Connection) -> anyhow::Result<u32> {
        let mut remaining = conn.prepare("SELECT qty FROM order_stock WHERE stock_id = ?")?;
        let consumed = remaining
            .query_map([&self.code], |r| Ok(r.get::<_, u32>(0)?))?
            .sum::<rusqlite::Result<u32>>()?;

        Ok(self.qty.saturating_sub(consumed))
    }

    pub fn consume(&self, qty: u32, for_order_id: &str, trans: &Transaction) -> anyhow::Result<()> {
        let remaining = self.remaining(trans)?;

        if qty > remaining {
            return Err(anyhow!(
                "Could not consume stock '{}': qty {} exceeds remaining count {}",
                self.code,
                qty,
                remaining
            ));
        }

        trans.execute(
            "INSERT INTO order_stock (order_id, stock_id, qty) VALUES (?, ?, ?)",
            params![&for_order_id, &self.code, &qty],
        )?;
        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct Order {
    pub id: Uuid,
    pub order_ts: OffsetDateTime,
    pub email: String,
    pub phone_number: String,
    pub address: String,
    pub payment_intent_id: String,
    pub stock: Vec<(String, u32)>,
}

impl Order {
    fn insert(&self, conn: &mut Connection) -> anyhow::Result<()> {
        let id = self.id.to_string();
        let trans = conn.transaction_with_behavior(TransactionBehavior::Immediate)?;

        trans.execute(
            "INSERT INTO orders (id, order_ts, email, phone_number, address, payment_intent_id) VALUES (?, ?, ?, ?, ?, ?)",
            params![&id, &self.order_ts, &self.email, &self.phone_number, &self.address, &self.payment_intent_id]
        )?;

        for (stock_code, qty) in self.stock.iter() {
            let mut stock = Stock::get(&stock_code, &trans)?;
            stock.consume(*qty, &id, &trans)?;
        }

        trans.commit()?;

        Ok(())
    }

    fn confirm(&self, conn: &mut Connection) -> anyhow::Result<()> {
        let id = self.id.to_string();
        let rows = conn.execute("UPDATE orders SET confirmed = TRUE WHERE id = ?", &[&id])?;
        if rows != 1 {
            return Err(anyhow!("tried to confirm nonexistent order {id}"));
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::db::{Order, Stock};
    use rusqlite::{Connection, TransactionBehavior};
    use std::sync::{Arc, Barrier};
    use time::OffsetDateTime;
    use uuid::Uuid;

    static SCHEMA: &'static str = include_str!("../schema.sql");

    fn setup(name: &str, init: bool) -> Connection {
        let temp = std::env::temp_dir();
        let dir = temp.join("panelpos-test");

        std::fs::create_dir_all(&dir).unwrap();

        let file = dir.join(name);

        if init {
            let _ = std::fs::remove_file(&file);
        }

        let conn = Connection::open(file).unwrap();

        if init {
            conn.execute_batch(SCHEMA).unwrap();
        }
        conn
    }

    #[test]
    fn cheese_accounting() {
        let mut conn = setup("cheese_accounting", true);

        // don't make me worry about orders yet
        conn.execute_batch("PRAGMA foreign_keys = OFF;").unwrap();

        conn.execute("INSERT INTO stock (code, name, price_pence, qty) VALUES (\"cheese\", \"Wensleydale?\", 100, 3)", [])
            .unwrap();

        // Test basic functionality:
        let cheese = Stock::get("cheese", &conn).unwrap();

        assert_eq!(
            cheese,
            Stock {
                code: "cheese".to_string(),
                name: "Wensleydale?".to_string(),
                price_pence: 100,
                qty: 3,
            }
        );

        assert_eq!(cheese.remaining(&conn).unwrap(), 3);

        // Consume one unit of cheese.
        let trans = conn.transaction().unwrap();
        cheese.consume(1, "abc", &trans).unwrap();
        trans.commit().unwrap();

        assert_eq!(cheese.remaining(&conn).unwrap(), 2);

        // Consume the rest of the cheese.
        let trans = conn.transaction().unwrap();
        cheese.consume(2, "def", &trans).unwrap();
        trans.commit().unwrap();

        assert_eq!(cheese.remaining(&conn).unwrap(), 0);

        // Check that we can't consume any more.
        let trans = conn.transaction().unwrap();
        assert!(cheese.consume(1, "ghi", &trans).is_err());
        trans.rollback().unwrap();
    }

    // This is probably a little too much effort put into a test, but hey ho
    #[test]
    fn cheese_race_condition() {
        for n in 0..20 {
            let mut conn = setup(&format!("cheese_race_condition-{n}"), true);
            conn.execute_batch("PRAGMA foreign_keys = OFF;").unwrap();

            conn.execute("INSERT INTO stock (code, name, price_pence, qty) VALUES (\"cheese\", \"Wensleydale?\", 100, 3)", [])
                .unwrap();

            fn cheese_consumption(conn: &mut Connection, for_order_id: &str) -> anyhow::Result<()> {
                let cheese = Stock::get("cheese", conn)?;
                let trans = conn.transaction_with_behavior(TransactionBehavior::Immediate)?;
                cheese.consume(2, for_order_id, &trans)?;
                trans.commit()?;
                Ok(())
            }

            let barrier = Arc::new(Barrier::new(2));
            let barrier_c = barrier.clone();

            let thread = std::thread::spawn(move || {
                let mut conn = setup(&format!("cheese_race_condition-{n}"), false);
                conn.execute_batch("PRAGMA foreign_keys = OFF;").unwrap();
                barrier_c.wait();
                cheese_consumption(&mut conn, "thread2")
            });

            barrier.wait();
            let ours = cheese_consumption(&mut conn, "thread1");
            let theirs = thread.join().unwrap();

            match (ours, theirs) {
                (Ok(_), Err(e)) | (Err(e), Ok(_)) => {
                    assert_eq!(
                        &e.to_string(),
                        "Could not consume stock 'cheese': qty 2 exceeds remaining count 1"
                    )
                }
                (Ok(_), Ok(_)) => panic!("concurrent cheese consumption!"),
                (Err(e1), Err(e2)) => panic!("no cheese consumption!\ne1: {e1:?}\ne2: {e2:?}"),
            }
        }
    }

    #[test]
    fn basic_orders() {
        let mut conn = setup("basic_orders", true);

        conn.execute("INSERT INTO stock (code, name, price_pence, qty) VALUES (\"cheese\", \"Wensleydale?\", 100, 3)", [])
            .unwrap();
        let cheese = Stock::get("cheese", &conn).unwrap();
        conn.execute(
            "INSERT INTO stock (code, name, price_pence, qty) VALUES (\"crackers\", \"No crackers, Gromit!!\", 100, 0)",
            [],
        )
            .unwrap();
        let crackers = Stock::get("crackers", &conn).unwrap();
        conn.execute(
            "INSERT INTO stock (code, name, price_pence, qty) VALUES (\"tasty-snacks\", \"Mmm, delicious\", 100, 5)",
            [],
        )
        .unwrap();
        let snacks = Stock::get("tasty-snacks", &conn).unwrap();

        // Basic order functionality:
        let mut wallace = Order {
            id: Uuid::new_v4(),
            order_ts: OffsetDateTime::now_utc(),
            email: "wallace@moon.invalid".to_string(),
            phone_number: "123".to_string(),
            address: "Wallace\nGrand Day Out Experiencing Co.\nThe Moon".to_string(),
            payment_intent_id: "stripe_zone".to_string(),
            stock: vec![("cheese".into(), 1), ("tasty-snacks".into(), 1)],
        };

        assert_eq!(cheese.remaining(&conn).unwrap(), 3);
        assert_eq!(snacks.remaining(&conn).unwrap(), 5);

        wallace.insert(&mut conn).unwrap();

        assert_eq!(cheese.remaining(&conn).unwrap(), 2);
        assert_eq!(snacks.remaining(&conn).unwrap(), 4);

        wallace.confirm(&mut conn).unwrap();

        // Orders fail if they can't have one of the things inside:
        let mut wallace_with_cracker_cravings = Order {
            id: Uuid::new_v4(),
            order_ts: OffsetDateTime::now_utc(),
            email: "wallace@moon.invalid".to_string(),
            phone_number: "123".to_string(),
            address: "Wallace\nGrand Day Out Experiencing Co.\nThe Moon".to_string(),
            payment_intent_id: "stripe_zone".to_string(),
            stock: vec![("cheese".into(), 1), ("crackers".into(), 1)],
        };

        assert_eq!(cheese.remaining(&conn).unwrap(), 2);

        match wallace_with_cracker_cravings.insert(&mut conn) {
            Ok(()) => panic!("incorrect"),
            Err(e) => assert_eq!(
                e.to_string(),
                "Could not consume stock 'crackers': qty 1 exceeds remaining count 0"
            ),
        }

        assert_eq!(cheese.remaining(&conn).unwrap(), 2);
    }
}
